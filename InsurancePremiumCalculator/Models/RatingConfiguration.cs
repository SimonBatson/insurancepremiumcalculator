﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsurancePremiumCalculator.Models
{
    public class RatingConfiguration
    {
        public const string SectionName = "RatingConfiguration";
        public const string OccupationSectionName = "Occupations";
        public const string OccupationRatingSectionName = "OccupationRatings";
        public const string RatingFactorSectionName = "RatingFactors";
        public IList<Occupation> Occupations = new List<Occupation>();
        public IList<RatingFactor> OccupationRatings = new List<RatingFactor>();

        public class Occupation
        {

            public string Name { get; set; }
            public int OccupationId { get; set; }
        }

        public class OccupationRating
        {
            public int OccupationId { get; set; }
            public int RatingId { get; set; }
        }
        public class RatingFactor
        {
            public int RatingId { get; set; }
            public decimal Factor { get; set; }
        }
    }
}
