﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsurancePremiumCalculator.Models
{
    public class Calculator
    {
        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        [Range(18, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Age { get; set; }

        [Display(Name = "Date of Birth", Prompt ="Date of birth")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        
        [Required(ErrorMessage = "Please select an occupation")]
        public int Occupation { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:C}")]
        [Display(Name = "Sum Insured")]
        [Range(1, 10000000, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int SumInsured { get; set; }
        
        [Editable(false)]
        [DisplayFormat(DataFormatString = "{0:C}")]
        [Display(Name = "Monthly Premium")]
        public decimal? Premium { get; set; }

    }
}
