﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsurancePremiumCalculator.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace InsurancePremiumCalculator.Controllers
{
    
    public class CalculatorController : Controller
    {
        private readonly IOptions<List<RatingConfiguration.OccupationRating>> _occupationRatings;
        private readonly IOptions<List<RatingConfiguration.Occupation>> _occupations;
        private readonly IOptions<List<RatingConfiguration.RatingFactor>> _ratingFactors;

        public CalculatorController(IOptions<List<RatingConfiguration.OccupationRating>> occupationRatings, IOptions<List<RatingConfiguration.Occupation>> occupations, IOptions<List<RatingConfiguration.RatingFactor>> ratingFactors)
        {
            _occupationRatings = occupationRatings;
            _occupations = occupations;
            _ratingFactors = ratingFactors;
        }

        // GET: CalculatorController
        public ActionResult Index()
        {
            return View();
        }

        // GET: CalculatorController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CalculatorController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CalculatorController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CalculatorController/Edit/5
        public ActionResult Edit(int id)
        {
            BuildOccupationList();
            return View();
        }

        // POST: CalculatorController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                int occupationRating = 0;
                bool fieldValid = int.TryParse(collection["Occupation"], out occupationRating);
                if (!fieldValid)
                    occupationRating = 0;
                int sumInsured = 0;
                fieldValid = int.TryParse(collection["SumInsured"], out sumInsured);
                if (!fieldValid)
                    sumInsured = 0;
                DateTime dateOfBirth;
                fieldValid = DateTime.TryParse(collection["DateOfBirth"], out dateOfBirth);
                if (!fieldValid)
                    dateOfBirth = DateTime.Today;
                int age = CalculateAge(dateOfBirth);
                decimal premium = CalculatePremium(sumInsured, occupationRating, age);
                BuildOccupationList();
                Models.Calculator calculator = new Calculator() { Age = age, DateOfBirth = dateOfBirth, Name = collection["Name"],  SumInsured = sumInsured, Occupation = occupationRating, Premium = premium };
                return View(calculator);
            }
            catch
            {
                return View();
            }
        }

        // GET: CalculatorController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CalculatorController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public decimal GetOccupationRatingFactor(int occupationId)
        {

            var rating =  _occupationRatings.Value.Where(x => x.OccupationId == occupationId).FirstOrDefault();
            if (rating == null)
                return 0m;
            var ratingFactor = _ratingFactors.Value.Where(x => x.RatingId == rating.RatingId).FirstOrDefault();
            decimal factor = (ratingFactor == null) ? 0 : ratingFactor.Factor;
            return factor;
        }

        public decimal CalculatePremium(int coverAmount, int occupationRating, int age)
        {
            decimal premium = 0;
            decimal occupationRatingFactor = GetOccupationRatingFactor(occupationRating);
            premium = (coverAmount * occupationRatingFactor * age) / 1000 * 12;
            return premium;
        }

        public int CalculateAge(DateTime dateOfBirth)
        {
            int age = DateTime.Today.Year - dateOfBirth.Year;
            return age;
        }

        private void BuildOccupationList()
        {
            var dropdownData = new List<SelectListItem>();
            _occupations.Value.ForEach(x => {
                dropdownData.Add(new SelectListItem(x.Name, x.OccupationId.ToString()));
            });

            ViewBag.Options = dropdownData;

        }

    }

}
