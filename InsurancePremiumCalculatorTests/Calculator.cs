using System;
using Xunit;
using InsurancePremiumCalculator;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;

namespace InsurancePremiumCalculatorTests
{
    public class Calculator
    {
        [Fact]
        public void GetOccupationRatingFactorBad()
        {
            //Arrange
            int occupationRating = 999;
            InsurancePremiumCalculator.Controllers.CalculatorController controller = setUpController();

            // Act
            var result = controller.GetOccupationRatingFactor(occupationRating);

            // Assert
            Assert.Equal(0, result);


        }

        [Fact]
        public void GetOccupationRatingFactor1()
        {
            //Arrange
            int occupationRating = 1;
            InsurancePremiumCalculator.Controllers.CalculatorController controller = setUpController();

            // Act
            var result = controller.GetOccupationRatingFactor(occupationRating);

            // Assert
            Assert.Equal(1.0m, result);


        }

        [Fact]
        public void GetOccupationRatingFactor3()
        {
            //Arrange
            int occupationRating = 3;
            InsurancePremiumCalculator.Controllers.CalculatorController controller = setUpController();

            // Act
            var result = controller.GetOccupationRatingFactor(occupationRating);

            // Assert
            Assert.Equal(1.5m, result);


        }

        [Fact]
        public void CalculatePremium()
        {
            //Arrange
            InsurancePremiumCalculator.Controllers.CalculatorController controller = setUpController();

            // Act
            var result = controller.CalculatePremium(100, 1, 10);

            // Assert
            Assert.Equal(12.0m, result);

        }

        [Fact]
        public void CalculateAge()
        {
            //Arrange
            InsurancePremiumCalculator.Controllers.CalculatorController controller = setUpController();
            DateTime dateOfBirth = DateTime.Today.AddYears(-30);
            // Act
            var result = controller.CalculateAge(dateOfBirth);

            // Assert
            Assert.Equal(30, result);

        }

        private IOptions<List<InsurancePremiumCalculator.Models.RatingConfiguration.Occupation>> SetUpOccupations()
        {
            List<InsurancePremiumCalculator.Models.RatingConfiguration.Occupation> occupations = new List<InsurancePremiumCalculator.Models.RatingConfiguration.Occupation>();
            occupations.Add(new InsurancePremiumCalculator.Models.RatingConfiguration.Occupation() { Name = "Occupation1", OccupationId = 1 });
            occupations.Add(new InsurancePremiumCalculator.Models.RatingConfiguration.Occupation() { Name = "Occupation2", OccupationId = 2 });
            occupations.Add(new InsurancePremiumCalculator.Models.RatingConfiguration.Occupation() { Name = "Occupation3", OccupationId = 3 });

            return Options.Create(occupations);
        }

        private IOptions<List<InsurancePremiumCalculator.Models.RatingConfiguration.OccupationRating>> SetUpOccupationRatings()
        {
            List<InsurancePremiumCalculator.Models.RatingConfiguration.OccupationRating> occupationRatings = new List<InsurancePremiumCalculator.Models.RatingConfiguration.OccupationRating>();
            occupationRatings.Add(new InsurancePremiumCalculator.Models.RatingConfiguration.OccupationRating() { OccupationId = 1, RatingId = 1 });
            occupationRatings.Add(new InsurancePremiumCalculator.Models.RatingConfiguration.OccupationRating() { OccupationId = 2, RatingId = 2 });
            occupationRatings.Add(new InsurancePremiumCalculator.Models.RatingConfiguration.OccupationRating() { OccupationId = 3, RatingId = 3 });

            return Options.Create(occupationRatings);
        }

        private IOptions<List<InsurancePremiumCalculator.Models.RatingConfiguration.RatingFactor>> SetUpRatingFactors()
        {
            List<InsurancePremiumCalculator.Models.RatingConfiguration.RatingFactor> ratingFactos = new List<InsurancePremiumCalculator.Models.RatingConfiguration.RatingFactor>();
            ratingFactos.Add(new InsurancePremiumCalculator.Models.RatingConfiguration.RatingFactor() { RatingId = 1, Factor = 1.0m });
            ratingFactos.Add(new InsurancePremiumCalculator.Models.RatingConfiguration.RatingFactor() { RatingId = 2, Factor = 1.25m });
            ratingFactos.Add(new InsurancePremiumCalculator.Models.RatingConfiguration.RatingFactor() { RatingId = 3, Factor = 1.5m });

            return Options.Create(ratingFactos);
        }

        private InsurancePremiumCalculator.Controllers.CalculatorController setUpController()
        {
            InsurancePremiumCalculator.Controllers.CalculatorController controller = new InsurancePremiumCalculator.Controllers.CalculatorController(SetUpOccupationRatings(), SetUpOccupations(), SetUpRatingFactors());
            return controller;
        }
    }
}
