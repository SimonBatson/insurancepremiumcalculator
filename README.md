# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone repository and open solution in Visual Studio
* AppSettings contains occupation and rate factor values
* Age is a function of date of birth and as such is calculated from date of birth. The age entered on the screen is ignored.
* Age is limited to the range 18 to 100
* Sum insured is limited to 10,000,000 and must be positive

